scikit-learn==1.2.1
pandas==1.3.5
matplotlib==3.5.1
numpy==1.21.5
scipy==1.7.3
findpeaks==2.4.3