# -*- coding: utf-8 -*-
"""
Created on Wed Feb 22 15:40:06 2023

@author: adria036
"""



#%% import packages
import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\ddhtAI\scripts\gait_features") 

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
import gait_feat_funct as gf
from sklearn.linear_model import RANSACRegressor
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline


#%% load data

# plotbool : if plotbool = True, make and save plots. Otherwise create just output scripts
plotbool = False   #True
# if you want to plot dynamically (which you need with plotbool == True), run without #
# %matplotlib qt

# set output path for saving results
path_out = os.path.join(
                    "C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research",
                    "iAdriaens_doc","Projects","cKamphuis",
                    "ddhtAI","results","preprocessing2"
                    )

# load data of keypoints
path = os.path.join(
                    "W:","/ASG","WLR_Dataopslag","Genomica",
                    "Sustainable_breeding", "44 0000 2700 KB DDHT AI 2020",
                    "6. data","T-LEAP","Data","Data for Tomas","Test_Tleap",
                    "Results"
                    )

# read data
data = pd.DataFrame([])
T=1
for folder in os.listdir(path):
    new = pd.read_csv(os.path.join(path,folder,"upscale_keypoints.csv"), 
                      index_col = 0,sep = ";")
    new["cow"] = T
    T = T+1
    data = pd.concat([data,new])
    del new
del path
data = data.reset_index(drop=1)

# height of frames in pixels  1520-2688
frameheight = 1520
framewidth = 2688

# correct cols in y direction for the fact origin is left upper corner
keypoints = data.columns[2:-1:2].str.replace("_x","").tolist()
for keypoint in keypoints:
    data[keypoint+"_y"] = -data[keypoint+"_y"] + frameheight
del keypoint, keypoints

""" 
# Potential improvement
# delete key points when x-value is below 500 or above 2688-230=2458
# the model does not work well in the corners - this can be corrected via the model

keypoints = data.columns[2:-1:2].str.replace("_x","").tolist()
for keypoint in keypoints:
    data.loc[data[keypoint+"_x"] < 500, keypoint+"_y"] = np.nan
    data.loc[data[keypoint+"_x"] < 500, keypoint+"_x"] = np.nan
    data.loc[data[keypoint+"_x"] > 2458, keypoint+"_y"] = np.nan
    data.loc[data[keypoint+"_x"] > 2458, keypoint+"_x"] = np.nan
del keypoint, keypoints
"""  
del frameheight, framewidth, T, folder

#%% visualise the data
if plotbool == True:
    cows = data["cow"].drop_duplicates().reset_index(drop=1)
    keypoints = data.columns[2:-1:2].str.replace("_x","").tolist()
    
    for cow in cows:
        subset = data.loc[data["cow"]==cow,:]
        for keypoint in keypoints:
            print(keypoint)
            fig,ax = plt.subplots(figsize = (20,12), nrows = 3, ncols = 1)
            ax[0].plot(subset["frame"],subset[keypoint+'_x'],'x-',color ="r",markersize = 3,markeredgewidth=2)
            ax[1].plot(subset["frame"],subset[keypoint+'_y'],'x-',color ="b",markersize = 3,markeredgewidth=2)
            ax[2].plot(subset[keypoint+'_x'],subset[keypoint+'_y'],'x-',color ="indigo",markersize = 3,markeredgewidth=2)
            ax[0].set_title(keypoint + ", cow = " + str(cow))
            ax[0].set_ylabel("x-values")
            ax[1].set_ylabel("y-values")
            ax[2].set_ylabel("y-values")
            ax[0].set_xlabel("frame")
            ax[1].set_xlabel("frame")
            ax[2].set_xlabel("x-values")
            
            plt.savefig(os.path.join(path_out,str(cow) + "_" + keypoint + "_no_corr.png"))
            plt.close()
    
    del ax, fig, subset, keypoints, cows

#%% data preprocessing step 1: correct y-value distortion
# RANSAC for fit with strong outliers in the y direction
# from sklearn.linear_model import HuberRegressor


cows = data["cow"].drop_duplicates().reset_index(drop=1)
keypoints = data.columns[2:18*2:2].str.replace("_x","").tolist()
ransac = RANSACRegressor(residual_threshold = 50)

for cow in cows:
    subset = data.loc[data["cow"] == cow,:]
    
    for keypoint in keypoints:
        X = subset["frame"].values
        X = X[:,np.newaxis]
        y = subset.loc[subset[keypoint + '_y'].isna()== False, keypoint + "_y"].values
        model = make_pipeline(PolynomialFeatures(2),ransac)
        model.fit(X,y)
        y_pred = model.predict(X)
        
        # add to data
        subset[keypoint + "_cy"] = y-y_pred + y_pred.mean()
        data.loc[subset.index.values,keypoint + "_cy"] = subset[keypoint + "_cy"]
        
        if plotbool == True:
            mad = 50
        
            fig,ax = plt.subplots(figsize = (20,6), nrows = 1, ncols = 1)
            ax.plot(subset["frame"],subset[keypoint+'_y'],'x',color ="b",markersize =5,markeredgewidth=2)
            ax.plot(subset["frame"].values,y_pred,'-',color ="r",linewidth=2)
            ax.plot(subset["frame"].values,y_pred.mean() * np.ones((len(y),1)),'--',color ="r",linewidth=2)
            ax.plot(subset.loc[np.abs(subset[keypoint + "_y"]-y_pred) > mad, "frame"],
                    subset.loc[np.abs(subset[keypoint + "_y"]-y_pred) > mad, keypoint + "_y"],'x',color ="r",markersize =5,markeredgewidth=2)

            ax.plot(subset["frame"],subset[keypoint+'_cy'],'x',color ="g",markersize =5,markeredgewidth=2)
            ax.set_title('cow = ' + str(cow) + ", RANSAC parabola-corrected y of " + keypoint)
            plt.savefig(os.path.join(path_out,str(cow) + "_" + keypoint + "_parabola.png"))
            plt.close()
            del ax, fig, mad
        
        del X,y,model,y_pred

del keypoint, keypoints, cow, cows, ransac, subset

#%% data preprocessing -- correct backwards x-values and y values that are >10% diff 

cows = data["cow"].drop_duplicates().reset_index(drop=1)
keypoints = data.columns[2:2*18:2].str.replace("_x","").tolist()

for cow in cows:
    subset = data.loc[data["cow"] == cow,:]
    for keypoint in keypoints:
        print(cow,keypoint)
                
        # calculate median, set to median if value > 100 pixels difference
        m = subset[keypoint+"_cy"].median()
        subset.loc[(subset[keypoint+"_cy"] > m+100) | \
                   (subset[keypoint+"_cy"] < m-100), keypoint+"_x"] = np.nan
        subset.loc[(subset[keypoint+"_cy"] > m+100) | \
                   (subset[keypoint+"_cy"] < m-100), keypoint+"_cy"] = m
        
        if plotbool == True:
            # plot raw data
            fig,ax = plt.subplots(figsize = (20,12), nrows = 3, ncols = 1)
            ax[0].plot(subset["frame"],subset[keypoint+'_x'],'x',color ="r",markersize =5,markeredgewidth=2)
            ax[1].plot(subset["frame"],subset[keypoint+'_cy'],'x',color ="r",markersize =5,markeredgewidth=2)
            ax[2].plot(subset[keypoint+'_x'],subset[keypoint+'_cy'],'x',color ="r",markersize = 5,markeredgewidth=2)
        
            # plot median corrected data
            ax[0].plot(subset["frame"],subset[keypoint+'_x'],'x',color ="b",markersize =5,markeredgewidth=2)
            ax[1].plot(subset["frame"],subset[keypoint+'_cy'],'x',color ="b",markersize =5,markeredgewidth=2)
            ax[2].plot(subset[keypoint+'_x'],subset[keypoint+'_cy'],'x',color ="b",markersize = 5,markeredgewidth=2)
        
        # fill na x with previous x position
        subset[keypoint+"_x"].fillna(inplace=True,method = "ffill") 
        
        # find values in which x < 15 (all except back) or < 50 (back)
        if keypoint not in ["Withers","Sacrum","Caudal thoracic"] :
            subset.loc[subset[keypoint + "_x"].diff() < -15,keypoint +"_x"] = np.nan
        else:
            subset.loc[subset[keypoint + "_x"].diff() < -50,keypoint +"_x"] = np.nan
        
        # fill na x with previous x position
        subset[keypoint+"_x"].fillna(inplace=True,method = "ffill") 
        
        # detect and remove severe outliers
        x = pd.DataFrame(savgol_filter(subset[keypoint+"_x"], 11, 2, mode = "nearest" ),columns = ["x"])
        x["d"] = np.abs(x["x"]-subset[keypoint+"_x"].values)
        x.index = subset.index
        subset.loc[x["d"]>100,keypoint+'_x'] = x.loc[x["d"]>100,"x"]
        
        x = pd.DataFrame(savgol_filter(subset[keypoint+"_x"], 9, 2, mode = "nearest" ),columns = ["x"])
        
        if plotbool == True:
            ax[0].plot(subset["frame"],x,'-',color ="r",linewidth = 1.5)
        
        
        # calculate median, set to median if value > 50 pixels difference
        m = subset[keypoint+"_cy"].median()
        subset.loc[(subset[keypoint+"_cy"] > m+100) | \
                   (subset[keypoint+"_cy"] < m-100), keypoint+"_cy"] = m
        
        y = savgol_filter(subset[keypoint+"_cy"], 7, 3, mode = "nearest" )
        
        if plotbool == True:
            ax[1].plot(subset["frame"],subset[keypoint+'_cy'],'x',color ="k",markersize =5,markeredgewidth=2)
            ax[1].plot(subset["frame"],y,'-',color ="r",linewidth = 1.5)
            ax[2].plot(subset[keypoint+'_x'],subset[keypoint+'_cy'],'x',color ="k",markersize = 5,markeredgewidth=2)
            ax[2].plot(x,y,'-',color ="indigo",linewidth = 1.5)
            ax[0].set_title(keypoint + ", cow = " + str(cow))
            ax[0].set_ylabel("x-values")
            ax[1].set_ylabel("yc-values")
            ax[2].set_ylabel("yc-values")
            ax[0].set_xlabel("frame")
            ax[1].set_xlabel("frame")
            ax[2].set_xlabel("x-values")

            plt.savefig(os.path.join(path_out,str(cow) + "_" + keypoint + "_corr.png"))
            plt.close()   
            del fig, ax
        
        # store x and y corrected in data
        data.loc[subset.index.values,keypoint + "_cx"] = x.values
        data.loc[subset.index.values,keypoint + "_cy"] = y
        
del subset, cow, cows, keypoint, keypoints, m, x, y


#%% gait features based on xc and yc
#import gait_feat_funct as gf
# cow = 1

cows = data["cow"].drop_duplicates().reset_index(drop=1)

# leg-based features
for cow in cows:
    # prepare output dataset for this cow
    df = data.loc[data["cow"] == cow,:].copy()
    df = df[df.columns[(((df.columns.str.contains("LH hoof")) & \
                        (df.columns.str.contains("_c"))) | \
                        ((df.columns.str.contains("RH hoof")) & \
                        (df.columns.str.contains("_c"))) | \
                        ((df.columns.str.contains("RF hoof")) & \
                        (df.columns.str.contains("_c"))) | \
                        ((df.columns.str.contains("LF hoof")) & \
                        (df.columns.str.contains("_c"))) | \
                        (df.columns.str.contains("frame"))) ]].copy()   
    df = df.reset_index(drop=1)
    
    # define x, y and t = frame
    x_LH = df["LH hoof_cx"]
    y_LH = df["LH hoof_cy"]
    x_RH = df["RH hoof_cx"]
    y_RH = df["RH hoof_cy"]
    x_LF = df["LF hoof_cx"]
    y_LF = df["LF hoof_cy"]
    x_RF = df["RF hoof_cx"]
    y_RF = df["RF hoof_cy"]
    t = df["frame"]
    
    # calculate where keypoint is stationary
    #    based on filtered, smoothed and imputed keypoint data
    # %matplotlib qt
    if plotbool == True:
        results_LH = gf.stat_hoof(x_LH,y_LH,t,True) # LH stationary
        fig = plt.gcf()
        plt.savefig(os.path.join(path_out,"gait_features","LH_" + str(cow) + "_" + "stathoof.png"))
        plt.close()
        
        results_RH = gf.stat_hoof(x_RH,y_RH,t,True) # RH stationary
        fig = plt.gcf()
        plt.savefig(os.path.join(path_out,"gait_features","RH_" +str(cow) + "_" + "stathoof.png"))
        plt.close()
        
        results_LF = gf.stat_hoof(x_LF,y_LF,t,True) # LF stationary
        fig = plt.gcf()
        plt.savefig(os.path.join(path_out,"gait_features","LF_" +str(cow) + "_" + "stathoof.png"))
        plt.close()
        
        results_RF = gf.stat_hoof(x_RF,y_RF,t,True) # RF stationary
        fig = plt.gcf()
        plt.savefig(os.path.join(path_out,"gait_features","RF_" +str(cow) + "_" + "stathoof.png"))
        plt.close()
    else:
        results_LH = gf.stat_hoof(x_LH,y_LH,t,False) # LH stationary
        results_RH = gf.stat_hoof(x_RH,y_RH,t,False) # RH stationary
        results_LF = gf.stat_hoof(x_LF,y_LF,t,False) # LF stationary
        results_RF = gf.stat_hoof(x_RF,y_RF,t,False) # RF stationary
        
    # combine results in single dataframe
    statgait = gf.combine_hoof_stats(cow,results_LF,results_RF,results_LH,results_RH)
    
    if 'gaits' not in globals():
        gaits = statgait
    elif ('gaits' in globals()) and (len(gaits.loc[gaits["cow"]==cow]) == 0): 
        gaits = gaits.append(statgait).reset_index(drop=1)
        
    del results_LH, results_RH, results_LF, results_RF
    del x_LH,y_LH,x_RH, y_RH,y_LF,x_LF,x_RF,y_RF,t
    # del df, x, y, xnew, xnew2, ynew, ynew2, valleys, valno, tnew, rgba_color
    # del frameheight, framewidth, i, peaks, results, f,a,ax,fig
    # del d1, d2, dist

del df, cow, cows  
    
#%% gait features
#-----------------------------------------------------------------------------
#---------------------------calculate features--------------------------------

    # 0) prepare df with gait features
    # 1) calculate gait symmetry features = front leg symmetry, back leg symmetry
    # 2) calculate stride lenght
    # 3) calculate speed / cadence
    # 4) calculate tracking up
    # 5) calculate 3leg ground time
    
#-----------------------------------------------------------------------------
#import gait_feat_funct as gf
# prepare frames
gaitfeat = pd.DataFrame([],columns = ["cow", ])
stride_all = {}
cows = gaits["cow"].drop_duplicates()

new = data[["cow","video"]].drop_duplicates().reset_index(drop=1)
new["date"] = pd.to_datetime(new["video"].str[0:10])
new["time"] = pd.to_numeric(new["video"].str[11:13])


for cow in cows:
    # select data from gaits
    df = gaits.loc[gaits["cow"] == cow]
    print(cow)
    # calculate the strides
    strides = gf.strides(df)
    # add strides to dictonary
    stride_all["cow_"+str(cow)] = strides

    # calculates gait features per cow per video per hoof
    strides = gf.strides(df) 
    strides = pd.merge(strides,new, on = "cow")

    print(cow,strides)
    
    # append and return 
    strides.to_csv(os.path.join(path_out,"gf_" + strides["video"][0]+"_cow_" + str(cow) + ".txt" ))
    

    
                       