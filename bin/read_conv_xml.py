# -*- coding: utf-8 -*-
"""
Created on Wed Apr  6 12:01:24 2022

@author: adria036
------------------------------------------------------------------------------
This script takes the annotations as inputs and transfers them into 
coordinates (x,y) of pixel values.
Next, the keypoints are visualised and standardised where necessary.


------------------------------------------------------------------------------


------------------------------------------------------------------------------
"""


import xml.etree.ElementTree as ET


import elementpath
from xml.etree import ElementTree


root = ET.XML('<A><B1/><B2><C1/><C2/><C3/></B2></A>')
elementpath.select(root, '/A/B2/*')

#%% get-read data



# path + filename
fn = 'annotations.xml'

# get the tree (structure of the xml)
tree = ET.parse(fn)
root = tree.getroot()


#