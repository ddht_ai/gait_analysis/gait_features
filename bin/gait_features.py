# -*- coding: utf-8 -*-
"""
Created on Wed Apr  6 10:13:28 2022

@author: adria036

------------------------------------------------------------------------------
This script contains the functions to calculate gait features from the key 
points as defined in the T-LEAP paper of Rusello et al. (2022).
It can be applied on the  training dataset as well as on the model output.
When using model output, probably additional preprocessing steps are necessary. 
The functions in this script assume "accurate" key points.
------------------------------------------------------------------------------

Per gait feature, a separate function is defined.
 - fun 1


------------------------------------------------------------------------------

Step 0: load data (for development - in separate script)
Step 1: visualise keypoints over time
Step 2: develop features



"""


#%% import packages
import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\ddhtAI\scripts\gait_features") 

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
# from mpl_toolkits import mplot3d
import scipy.optimize
#import copy
from gait_feat_funct import non_uniform_savgol, parabola
import gait_feat_funct as gf
# import math
# from numpy.linalg import norm


#%% load data

# set path
# path = os.path.join(
#                     "C:","/Users","adria036",
#                     "OneDrive - Wageningen University & Research",
#                     "iAdriaens_doc","Projects","cKamphuis",
#                     "ddhtAI","scripts","gait_features","data"
#                     )
# # set filename
# fn = '\\vid_0_1.csv'

# # read csv into pd DateFrame
# data = pd.read_csv(path+fn, sep = ";")
# extract numeric values from frame and sort
# frameno = []
# for i in data["frame"]:
#     frameno.append(int(i[5:-4]))

# data["no"] = frameno
# data = data.sort_values(by = "no").reset_index(drop=1)

# set output path for saving results
path_out = os.path.join(
                    "C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research",
                    "iAdriaens_doc","Projects","cKamphuis",
                    "ddhtAI","results","preprocessing"
                    )

# load data of keypoints
path = os.path.join(
                    "W:","/ASG","WLR_Dataopslag","Genomica",
                    "Sustainable_breeding", "44 0000 2700 KB DDHT AI 2020",
                    "6. data","T-LEAP","Data","Annotations","labels_csv"
                    )

# load annotations_information to get vids for test set
keep_vids = ["vid_29_1.csv","vid_29_2.csv","vid_38_1.csv","vid_42_1.csv","vid_43_1.csv",
             "vid_44_1.csv","vid_31_1.csv","vid_45_1.csv","vid_47_1.csv","vid_49_1.csv",
             "vid_52_1.csv","vid_54_1.csv","vid_57_1.csv","vid_58_1.csv","vid_60_1.csv"]

i = 1
data = pd.DataFrame([])
for fn in os.listdir(path):
    if fn in keep_vids:
        print(fn)
        try:
            cow = pd.read_csv(path+"/" + fn,sep = ";")
            cow["cow"] = i
            data = data.append(cow)
            data = data.reset_index(drop=1)
            i += 1
        except:
            print(fn, "error")
            # do nothing read folder

# delete data without frame number information
frameno = []
for i in range(0,len(data)):
   
    j = data["frame"][i]
    try:
        frameno.append(int(j[5:-4]))
    except:
        try:
            frameno.append(int(j[0:-4]))
        except:
            try:
                frameno.append(int(j))
            except:
                print(data.loc[i+1,["frame","video"]])
                data = data.drop(i, axis = 0)
data = data.reset_index(drop=1)
data["frame"] = frameno

# sort
data = data.sort_values(by = ["cow","frame"]).reset_index(drop=1)

# drop columns
data = data.loc[:,~data.columns.str.contains("likelihood")]



#%% check and correct for non-plausible annotations / model outcomes
#    - if the previous keypoint = annotated on x-axis after current - correct
#      with previous = current

# for now, delete vids with errors = vid_30_1 and vid_0_ines_5fp
data = data.loc[data["video"] != "Keypoint_vid0_Ines_5fs",:]
data = data.loc[data["video"] != "vid_30_1",:]
data=data.reset_index(drop=1)            

# loop over all columns with an x value and correct if x > 15 pixels behind
cols = data.columns
cows = data["cow"].drop_duplicates()
data_new = pd.DataFrame([])
# cows = cows[15:]
for cow in cows:
    data_c = data.loc[data["cow"] == cow,:]
    for col in cols:
        if "_x" in col:
            print(cow,col)
            xdif = data_c[col].diff()
            xdif = xdif.to_frame(name = "xdif")
            xdif[col] = data_c[col]
            xdif["frame"] = data_c["frame"]
            # where xdif < -15 : set to NaN
            # keypoints not in the back: 15 pixels = threshold
            if col not in ["Withers_x","Sacrum_x","Caudal thoracic_x"] :
                xdif.loc[xdif["xdif"]<-15,col] = np.nan
            else:
                xdif.loc[xdif["xdif"]<-50,col] = np.nan
            
            # interpolate/smooth according to frame distance ("no") with Sav-Gol
            x = xdif.loc[xdif[col].isna() == False,"frame"].reset_index(drop=1)
            y = xdif.loc[xdif[col].isna() == False,col].reset_index(drop=1)
            test,test2 = non_uniform_savgol(x, y, 7, 2)
            ncol = col+"_m"
            test2.rename(columns={'mean':ncol},inplace = True)
            
            # figure plotting x, y and SG-smoothed x
            # fig, ax = plt.subplots(figsize = (20,8), nrows = 1, ncols = 1)
            # ax.plot(data_c["frame"],data_c[col],'x',color ="r",markersize = 11,markeredgewidth=3)
            # ax.plot(x,y,'o',color = 'b',markersize=11)
            # ax.plot(test2["frameno"],test2[ncol],':',color ='g', linewidth=2)
            # # ax.plot(x,test,'--')
            # ax.set_title("corrected x values , col =" + col + ", cow = " + str(cow))
            # ax.set_ylabel("x-values")
            # ax.set_xlabel("frame number")
            
            # plt.savefig(path_out + "//" +"testset_ann"+ str(cow) + "_" + col + "_x_corr")
            # # plt.close()
                        
            # add new data to data_c given frame
            data_c = data_c.merge(test2,how = 'inner',
                                  left_on = 'frame',
                                  right_on = 'frameno'
                                  )
            data_c = data_c.drop("frameno",axis=1)
    # add to new dataframe
    data_new = data_new.append(data_c)
                    
# # del variables to clear workspace
del col, cols, cow, data_c, cows, fn, frameno, ncol, test, test2
del x, y, xdif


#%% correct keypoints for upside down origin/y axis and distortion (parabola)

# correct for origin upside down and y-axis > intuition

# height of frames in pixels  1520-2688
frameheight = 1520
framewidth = 2688

# correct cols in y direction for the fact origin is left upper corner
cols = data_new.columns
for col in cols:
    if "_y" in col:
        print(col)
        data_new[col] = -data_new[col] + frameheight


# fit function per keypoint + average pars
cows = data["cow"].drop_duplicates()
data_fin = pd.DataFrame([])
cmap = mpl.cm.get_cmap('jet',16)

for cow in cows:
    dicts ={}
    counter = 0
    data_c = data_new.loc[data_new["cow"] == cow,:].copy()
    for col in cols:
        if "_y" in col:
            counter += 1
            y = data_c[col]
            x = data_c["frame"]
            p = scipy.optimize.curve_fit(parabola, x, y)
            p = p[0]
            dicts[col] = p
            colnew = col+'_m'
            data_c[colnew] = y-parabola(x,p[0],p[1],p[2]) + y.mean()
        
            # define color based on colnumber (no keypoints = 16)
            color = cmap(counter)

            # # plot per keypoint        
            # fig, ax = plt.subplots(figsize = (20,18), ncols = 1, nrows = 2)
            # plt.rcParams.update({'font.size': 22})
            # mark, = ax[0].plot(x,y,"o",
            #             markersize = 10,
            #             color = color)
            # ax[0].plot(x,parabola(x,p[0],p[1],p[2]),"--",
            #             linewidth = 3,
            #             color = mark.get_color())
            # ax[0].set_title("Cow " + str(cow) +", "+ col)
            # ax[0].set_xlabel("frameno")
            # ax[0].set_ylabel("y")
            # ax[0].set_ylim(0,frameheight)
            # # plot corrected
            # ax[1].plot(x,y-parabola(x,p[0],p[1],0), 'o--',
            #             markersize = 10,
            #             linewidth = 3,
            #             color = mark.get_color())
            
            # ax[1].set_title("Cow " + str(cow) + ", parabola-corrected "+col)
            # ax[1].set_xlabel("frameno")
            # ax[1].set_ylabel("y - corrected")
            # ax[1].set_ylim(0,frameheight)
            
            # # save plot 
            # plt.savefig(path_out + "//" + str(cow) + "_" + col + "_ycorr")
            # plt.close()
                        
    # add to new dataframe
    data_fin = data_fin.append(data_c)
    
del col, colnew, color, counter, cow, dicts, keep_vids, p, x,y


#%% Key points

# TODO: all keypoints per cow per "trait" in 3D
# TODO: max difference in hoofdbeweging
# 1) leg traits
# 2) head movement
# 3) back arching 

# 1) back arching
for cow in cows:
    print("back arching for cow " + str(cow))
    # prepare output dataset for this cow
    df = data_fin.loc[data_fin["cow"] == cow,:].copy()
    df = df[df.columns[(((df.columns.str.contains("Sacrum")) | \
                        (df.columns.str.contains("Caudal")) | \
                        (df.columns.str.contains("Withers"))) & \
                        (df.columns.str.contains("_m")) | \
                        (df.columns.str.contains("frame"))) ]].copy()
    df = df.reset_index(drop=1)
    df["BA_d"] = np.nan
    df["BA_ang"] = np.nan
    frames = df["frame"]
    
    # prepare x and y
    xall = df[["Sacrum_x_m","Caudal thoracic_x_m","Withers_x_m"]]
    yall = df[["Sacrum_y_m","Caudal thoracic_y_m","Withers_y_m"]]
    
    
    fig, ax = plt.subplots(figsize = (20,15), ncols = 1, nrows = 2)
    plt.rcParams.update({'font.size': 12})
    for i in range(0,len(frames),2):
        
        x = xall.iloc[i]
        x = x.to_numpy()
        y = yall.iloc[i]
        y = y.to_numpy()
        
        # calculate back arching parameters d and ang
        d,ang = gf.back_arch(x,y)
        
        # add to df
        df["BA_d"].iloc[i] = d
        df["BA_ang"].iloc[i] = ang
        
        # plot "features"   = angle and distance d      
        
        plt.rcParams.update({'font.size': 18})
        ax[0].plot([frames[i],frames[i]],[0,d],"-",
                        linewidth = 8,
                        color = 'b')
        ax[1].plot([frames[i],frames[i]],[0,ang],"-",
                    linewidth = 8,
                        color = 'g')
        ax[0].set_title("Cow " + str(cow) +", back-arching level")
        ax[1].set_title("Back-arching angle")
        ax[0].set_xlabel("frame / time")
        ax[0].set_ylabel("norm distance")
        ax[1].set_xlabel("frame / time")
        ax[1].set_ylabel("back angle")
        ax[1].set_ylim(min(df["BA_ang"])-10,max(df["BA_ang"])+10)
        ax[0].set_ylim(0,max(df["BA_d"]+2))
                            
        # save plot 
        plt.savefig(path_out + "//" + str(cow) + "_backarching.png")
        # plt.close()
        
#TODO: save df in keypoint DF

#del ang, ax, cols, d, fig, i, ncol, test, test2

# 2) Head bob


%matplotlib qt

cow = 1
# 3) leg-based features
for cow in cows:
    # prepare output dataset for this cow
    df = data_fin.loc[data_fin["cow"] == cow,:].copy()
    df = df[df.columns[(((df.columns.str.contains("LH hoof")) & \
                        (df.columns.str.contains("_m"))) | \
                        ((df.columns.str.contains("RH hoof")) & \
                        (df.columns.str.contains("_m"))) | \
                        ((df.columns.str.contains("RF hoof")) & \
                        (df.columns.str.contains("_m"))) | \
                        ((df.columns.str.contains("LF hoof")) & \
                        (df.columns.str.contains("_m"))) | \
                        (df.columns.str.contains("frame"))) ]].copy()   
    df = df.reset_index(drop=1)
    
    # define x, y and t = frame
    x_LH = df["LH hoof_x_m"]
    y_LH = df["LH hoof_y_m"]
    x_RH = df["RH hoof_x_m"]
    y_RH = df["RH hoof_y_m"]
    x_LF = df["LF hoof_x_m"]
    y_LF = df["LF hoof_y_m"]
    x_RF = df["RF hoof_x_m"]
    y_RF = df["RF hoof_y_m"]
    t = df["frame"]
    
    # calculate where keypoint is stationary
    #    based on filtered, smoothed and imputed keypoint data
    results_LH = gf.stat_hoof(df["LH hoof_x_m"],df["LH hoof_y_m"],df["frame"],True) # LH stationary
    results_RH = gf.stat_hoof(df["RH hoof_x_m"],df["RH hoof_y_m"],df["frame"],True) # RH stationary
    results_LF = gf.stat_hoof(df["LF hoof_x_m"],df["LF hoof_y_m"],df["frame"],True) # LF stationary
    results_RF = gf.stat_hoof(df["RF hoof_x_m"],df["RF hoof_y_m"],df["frame"],True) # RF stationary
    
    # combine results in single dataframe
    statgait = gf.combine_hoof_stats(cow,results_LF,results_RF,results_LH,results_RH)
    
    if 'gaits' not in globals():
        gaits = statgait
    elif ('gaits' in globals()) and (len(gaits.loc[gaits["cow"]==cow]) == 0): 
        gaits = gaits.append(statgait).reset_index(drop=1)
        
            

    del results_LH, results_RH, results_LF, results_RF
    del x_LH,y_LH,x_RH, y_RH,y_LF,x_LF,x_RF,y_RF,t
    # del df, x, y, xnew, xnew2, ynew, ynew2, valleys, valno, tnew, rgba_color
    # del frameheight, framewidth, i, peaks, results, f,a,ax,fig
    # del d1, d2, dist
    
#-----------------------------------------------------------------------------
#---------------------------calculate features--------------------------------

    # 0) prepare df with gait features
    # 1) calculate gait symmetry features = front leg symmetry, back leg symmetry
    # 2) calculate stride lenght
    # 3) calculate speed / cadence
    # 4) calculate tracking up
    # 5) calculate 3leg ground time
    
#-----------------------------------------------------------------------------
import gait_feat_funct as gf
# prepare frames
gaitfeat = pd.DataFrame([],columns = ["cow", ])
stride_all = {}
cows = gaits["cow"].drop_duplicates()
for cow in cows:
    # select data from gaits
    df = gaits.loc[gaits["cow"] == cow]
    print(cow)
    # calculate the strides
    strides = gf.strides(df)
    # add strides to dictonary
    stride_all["cow_"+str(cow)] = strides

    
    
    # from the strides, calculate the gait features
    

    #----------------------------- 1) symmetry -------------------------------
    #    difference in stride length, expressed in percentage
    #    percentage deals with differences in distance to camera
    
    
    
    #--------------------------- 2) stride length ----------------------------
    #    stride length for each leg separately (LH,RH,LF,RF)
    #    stride duration for each leg
    strides = gf.strides(df) 

    print(cow,strides)
    
    #-------------------------- 3) speed / cadence ---------------------------
    
    #---------------------------- 4) tracking up -----------------------------
    #    difference in x-position LEFT legs touching ground
    #    difference in x-position RIGHT legs touching ground
    
    
    #--------------------------5) 3-leg ground time --------------------------
    #    ground time for RH
    #    ground time for LH
    #    difference in ground time between both
    
    


for cow in cows:
    # prepare output dataset for this cow
    df = data_fin.loc[data_fin["cow"] == cow,:].copy()
    # df = 
        

    for i in range(len(x)):
        x1 = x[i]
        # y1 = 
     
        ###############################
    cols = df.columns[1:].values.tolist()
    cols = pd.DataFrame([el[:-4] for el in cols]).drop_duplicates().reset_index(drop=1)
    cols.values.tolist()
    cmap = mpl.cm.get_cmap('jet',len(cols))
    counter = -1    
    
    fig = plt.figure(figsize = (25,25))
    ax = plt.axes(projection = '3d')
    x = df["frame"].values
    
    for col in cols.values:
        counter += 1
        coly = col[0] + "_x_m"
        colz = col[0] + "_y_m"
        y = df[coly].values
        z = df[colz].values
    
        ax.scatter3D(x,y,z, color=cmap(counter))
        ax.set_xlabel("Frame/time")
        ax.set_ylabel("x-value")
        ax.set_zlabel("y-value")
        ax.set_zlim(0,frameheight)
        ax.set_ylim(0,framewidth)
        ax.set_xlim(min(x)-10, max(x)+10)
        ax.view_init(15,-10)
        ax.legend(["Nose","Forehead","Withers"])


df = data.loc[data["cow"] == cow,:].copy()
df = df[df.columns[(((df.columns.str.contains("Withers")) | \
                        (df.columns.str.contains("Nose")) | \
                        (df.columns.str.contains("Forehead"))) | \
                        (df.columns.str.contains("frame"))) ]]


#%% visualise keypoints

# prepare figure with leg keypoints -- hoof and fetlock
fig, axs = plt.subplots(figsize = (30,15), nrows = 4, ncols = 1)
axs[0].plot(data["LF hoof_x"],data["LF hoof_y"], 'o:',
            markersize = 4, color = 'darkmagenta')
axs[0].plot(data["LF fetlock_x"],data["LF fetlock_y"], 'o:',
            markersize = 4, color = 'magenta')
axs[1].plot(data["RF hoof_x"],data["RF hoof_y"], 'o:',
            markersize = 4, color = 'mediumvioletred')
axs[1].plot(data["RF fetlock_x"],data["RF fetlock_y"], 'o:',
            markersize = 4, color = 'palevioletred')
axs[2].plot(data["LH hoof_x"],data["LH hoof_y"], 'o:',
            markersize = 4,color = 'mediumblue')
axs[2].plot(data["LH fetlock_x"],data["LH fetlock_y"], 'o:',
            markersize = 4, color = 'blue')
axs[3].plot(data["RH hoof_x"],data["RH hoof_y"], 'o:',
            markersize = 4, color = 'dodgerblue')
axs[3].plot(data["RH fetlock_x"],data["RH fetlock_y"], 'o:', 
            markersize = 4, color = 'deepskyblue')
axs[0].set_title('LF')
axs[1].set_title('RF')
axs[2].set_title('LH')
axs[3].set_title('RH')
axs[0].set_ylabel('y')
axs[1].set_ylabel('y')
axs[2].set_ylabel('y')
axs[3].set_ylabel('y')
axs[3].set_xlabel('x')
axs[3].set_xlabel('x')
axs[0].legend(["hoof","fetlock"])
axs[1].legend(["hoof","fetlock"])
axs[2].legend(["hoof","fetlock"])
axs[3].legend(["hoof","fetlock"])

# one cow
data1= data_fin.loc[data_fin["cow"] == 17,:].copy()
fig, axs = plt.subplots(figsize = (30,18), nrows = 4, ncols = 1)
axs[0].plot(data1["LF hoof_x_m"],data1["LF hoof_y_m"], 'o:',
            markersize = 4, color = 'darkmagenta')
axs[0].plot(data1["LF fetlock_x_m"],data1["LF fetlock_y_m"], 'o:',
            markersize = 4, color = 'magenta')
axs[1].plot(data1["RF hoof_x_m"],data1["RF hoof_y_m"], 'o:',
            markersize = 4, color = 'mediumvioletred')
axs[1].plot(data1["RF fetlock_x"],data1["RF fetlock_y_m"], 'o:',
            markersize = 4, color = 'palevioletred')
axs[2].plot(data1["LH hoof_x_m"],data1["LH hoof_y_m"], 'o:',
            markersize = 4,color = 'mediumblue')
axs[2].plot(data1["LH fetlock_x_m"],data1["LH fetlock_y_m"], 'o:',
            markersize = 4, color = 'blue')
axs[3].plot(data1["RH hoof_x_m"],data1["RH hoof_y_m"], 'o:',
            markersize = 4, color = 'dodgerblue')
axs[3].plot(data1["RH fetlock_x_m"],data1["RH fetlock_y_m"], 'o:', 
            markersize = 4, color = 'deepskyblue')
axs[0].set_title('LF')
axs[1].set_title('RF')
axs[2].set_title('LH')
axs[3].set_title('RH')
axs[0].set_ylabel('y')
axs[1].set_ylabel('y')
axs[2].set_ylabel('y')
axs[3].set_ylabel('y')
axs[3].set_xlabel('x')
axs[3].set_xlabel('x')
axs[0].legend(["hoof","fetlock"])
axs[1].legend(["hoof","fetlock"])
axs[2].legend(["hoof","fetlock"])
axs[3].legend(["hoof","fetlock"])
axs[0].set_xlim(min(data1["RH hoof_x_m"])-150, max(data1["RF hoof_x_m"])+150)
axs[1].set_xlim(min(data1["RH hoof_x_m"])-150, max(data1["RF hoof_x_m"])+150)
axs[2].set_xlim(min(data1["RH hoof_x_m"])-150, max(data1["RF hoof_x_m"])+150)
axs[3].set_xlim(min(data1["RH hoof_x_m"])-150, max(data1["RF hoof_x_m"])+150)

# prepare figure with one leg only -- hoof and fetlock
fig, axs = plt.subplots(figsize = (20,12), nrows = 2, ncols = 1)
axs[0].plot(data["LF hoof_x"],data["LF hoof_y"], 'o:',
            markersize = 6, color = 'darkmagenta')
axs[0].plot(data["LF fetlock_x"],data["LF fetlock_y"], 'o:',
            markersize = 6, color = 'magenta')
axs[1].plot(data["no"],data["LF hoof_x"], 'o:',
            markersize = 6, color = 'darkmagenta')
axs[1].plot(data["no"],data["LF fetlock_x"], 'o:',
            markersize = 6, color = 'magenta')
axs[0].set_title('LF, x vs. y')
axs[1].set_title('LF, frameno vs. x')
axs[0].set_ylabel('y')
axs[1].set_ylabel('x')
axs[0].set_xlabel('x')
axs[1].set_xlabel('frameno')
axs[0].legend(["hoof","fetlock"])
axs[1].legend(["hoof","fetlock"])



# figure plotting x, y and SG-smoothed x
fig, ax = plt.subplots(figsize = (20,8), nrows = 1, ncols = 1)
ax.plot(data_c["no"],data_c[col],'x',color ="r",markersize = 11,markeredgewidth=3)
ax.plot(x,y,'o',color = 'b',markersize=11)
ax.plot(test2["frameno"],test2["mean"],':',color ='g', linewidth=2)
ax.set_title("corrected x values")
ax.set_ylabel("x-values")
ax.set_xlabel("frame number")


"""
Lessons learned from visualisation + mitigation of errors
    - You don't expect a keypoint to move backwards in the x direction
        => it's probably an error if it does; needs correction
    - the pixel origin is in the left upper corner, which is counter-intuitive
        => correct / standardise this in beforehand to make analysis intuitive
        => multiple options





"""


# for all cols - fit trend model (parabola) and correct y for distortion
x = pd.Series([],name = 'x', dtype = 'float')
y = pd.Series([],name = 'y', dtype = 'float')
for col in cols:
    if "_y" in col:
        y = pd.concat([y,data_c[col]])
        x = pd.concat([x,data_c["no"]])

dist_cor_data = pd.DataFrame({'x' : x, 'y' : y})
dist_cor_data = dist_cor_data.sort_values(by = 'x')

# option 1: fit function on all data points
p = scipy.optimize.curve_fit(parabola, dist_cor_data["x"], dist_cor_data["y"])
p = p[0]
dist_cor_data["model"] = parabola(dist_cor_data["x"],p[0],p[1],p[2])

# plot data and function
fig, ax = plt.subplots(figsize = (30,20))
ax.plot(dist_cor_data["x"],dist_cor_data["y"],"o",
           markersize = 4, color = 'darkcyan')
ax.plot(dist_cor_data["x"].drop_duplicates(),
        parabola(dist_cor_data["x"].drop_duplicates(),p[0],p[1],p[2]),"-",
           linewidth = 4, color = 'r')
