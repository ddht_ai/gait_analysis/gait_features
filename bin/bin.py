# -*- coding: utf-8 -*-
"""
Created on Mon Sep 26 10:21:58 2022

@author: adria036
"""



# unclean pieces of code

# HEAD NOSE WITHERS
for cow in cows:
    
    df = data_fin.loc[data_fin["cow"] == cow,:].copy()
    df = df[df.columns[(((df.columns.str.contains("Withers")) | \
                        (df.columns.str.contains("Nose")) | \
                        (df.columns.str.contains("Forehead"))) & \
                        (df.columns.str.contains("_m")) | \
                        (df.columns.str.contains("frame"))) ]]
    
    cols = df.columns[1:].values.tolist()
    cols = pd.DataFrame([el[:-4] for el in cols]).drop_duplicates().reset_index(drop=1)
    cols.values.tolist()
    cmap = mpl.cm.get_cmap('jet',len(cols)/2)
    counter = -1    
    
    fig = plt.figure(figsize = (25,25))
    plt.rcParams.update({'font.size': 15})
    ax = plt.axes(projection = '3d')
    x = df["frame"].values
    
    for col in cols.values:
        counter += 1
        coly = col[0] + "_x_m"
        colz = col[0] + "_y_m"
        y = df[coly].values
        z = df[colz].values
    
        ax.scatter3D(x,y,z, color=cmap(counter), s = 75)
        ax.set_xlabel("Frame/time")
        ax.set_ylabel("x-value")
        ax.set_zlabel("y-value")
        # ax.set_zlim(0,frameheight)
        ax.set_zlim(400,1200)
        # ax.set_ylim(0,framewidth)
        ax.set_ylim(500,framewidth)
        ax.set_xlim(min(x)-10, max(x)+10)
        ax.view_init(15,-10)
        ax.legend(["Nose","Forehead","Withers"])
        ax.set_title("Position of Nose-Forehead-Withers over time")
    
    
    
# distance withers- nose + head
#todo: needs normalisation for distance   
for cow in cows:
    df = data_fin.loc[data_fin["cow"] == cow,:].copy()
    df = df[df.columns[(((df.columns.str.contains("Sacrum")) | \
                        (df.columns.str.contains("Caudal")) | \
                        (df.columns.str.contains("Withers"))) & \
                        (df.columns.str.contains("_m")) | \
                        (df.columns.str.contains("frame"))) ]]
    df = df.reset_index(drop=1)
        
    frames = df["frame"]
    cmap = mpl.cm.get_cmap('cool',len(frames)/2)
    counter = -1    
    
    fig = plt.figure(figsize = (25,25))
    plt.rcParams.update({'font.size': 15})
    ax = plt.axes(projection = '3d')
    
    for i in range(0,len(frames),2):
        counter += 1
        
        y = df[["Sacrum_x_m","Caudal thoracic_x_m","Withers_x_m"]]
        y = y.iloc[i]
        y = y.to_numpy()
        z = df[["Sacrum_y_m","Caudal thoracic_y_m","Withers_y_m"]]
        z = z.iloc[i]
        z = z.to_numpy()
        x = np.repeat(df["frame"][i],z.size)
    
        ax.plot3D(x,y,z, color=cmap(counter))
        ax.scatter3D(x,y,z, color=cmap(counter), s = 75, depthshade = False)
        ax.set_xlabel("frame/time")
        ax.set_ylabel("x-value")
        ax.set_zlabel("y-value")
        ax.set_zlim(min(df["Sacrum_y_m"])-50,max(df["Sacrum_y_m"])+50)
        ax.set_ylim(min(df["Sacrum_x_m"])-50,max(df["Sacrum_x_m"])+50)
        ax.set_xlim(min(df["frame"])-50, max(df["frame"])+100)
        ax.view_init(15,-15)
        ax.set_title("Position of Sacrum-Caudal vertebrae-Withers over time, cow = " + str(cow))
        
        # save plot 
        plt.savefig(path_out + "//" + str(cow) + "_" + col + "3D_backarch.png")
        # plt.close()