"""
Created by Ines Adriaens, April 14, 2022
Created on Wed Mar  9 17:53:54 2022
@author: adria036, Ines Adriaens

-------------------------------------------------------------------------------

Functions belonging to postprocessing of gait features as calculated via
manual annotation or returned by T-LEAP model

-------------------------------------------------------------------------------

Functions:
- Savitsky-Golay filtering for unequally spaced data




"""

import numpy as np
import pandas as pd
from numpy.linalg import norm
import matplotlib.pyplot as plt
import matplotlib.colors as pltcol
from matplotlib import cm
from findpeaks import findpeaks


# parabola for correcting distortion
def parabola(x, a, b, c):
    return a*x**2 + b*x + c


def non_uniform_savgol(x, y, window, polynom):
    """
    Applies a Savitzky-Golay filter to y with non-uniform spacing
    as defined in x

    This is based on https://dsp.stackexchange.com/questions/1676/savitzky-golay-smoothing-filter-for-not-equally-spaced-data
    The borders are interpolated like scipy.signal.savgol_filter would do

    Parameters
    ----------
    x : array_like
        List of floats representing the x values of the data
    y : array_like
        List of floats representing the y values. Must have same length
        as x
    window : int (odd)
        Window length of datapoints. Must be odd and smaller than x
    polynom : int
        The order of polynom used. Must be smaller than the window size

    Returns
    -------
    np.array of float
        The smoothed y values
    """
    if len(x) != len(y):
        raise ValueError('"x" and "y" must be of the same size')

    if len(x) < window:
        raise ValueError('The data size must be larger than the window size')

    if type(window) is not int:
        raise TypeError('"window" must be an integer')

    if window % 2 == 0:
        raise ValueError('The "window" must be an odd integer')

    if type(polynom) is not int:
        raise TypeError('"polynom" must be an integer')

    if polynom >= window:
        raise ValueError('"polynom" must be less than "window"')

    half_window = window // 2
    polynom += 1

    # Initialize variables
    A = np.empty((window, polynom))     # Matrix
    tA = np.empty((polynom, window))    # Transposed matrix
    t = np.empty(window)                # Local x variables
    y_smoothed = np.full(len(y), np.nan)

    # Start smoothing
    N = 0
    for i in range(half_window, len(x) - half_window, 1):
        # print(i)
        N += 1
        
        # Center a window of x values on x[i]
        for j in range(0, window, 1):
            #print(j)
            t[j] = x[i + j - half_window] - x[i]
            
            #NEW: t2 with stepsize 1
            t2 = list(range(x[i - half_window] - x[i], x[i + (window-1) - half_window] - x[i]+1))
            t2 = np.asarray(t2,dtype = float)
        
        #NEW: Initialize variables based on variable window
        A2 = np.empty((len(t2), polynom))     # Matrix
        # tA2 = np.empty((polynom, len(t2)))    # Transposed matrix
            
        # Create the initial matrix A and its transposed form tA
        for j in range(0, window, 1):
            r = 1.0
            for k in range(0, polynom, 1):
                A[j, k] = r
                tA[k, j] = r
                r *= t[j]
                
        for j in range(0,len(t2),1):
            # print(j,t2[j])
            r = 1.0
            for k in range(0, polynom, 1):
                A2[j, k] = r
                # tA2[k, j] = r
                r *= t2[j]

        # Multiply the two matrices = (A^T*A)
        tAA = np.matmul(tA, A)

        # Invert the product of the matrices =(A^T*A)-1
        tAA = np.linalg.inv(tAA)

        # Calculate the pseudoinverse of the design matrix = (A^T*A)-1*A^T 
        coeffs = np.matmul(tAA, tA)
        
        #NEW: Calculate c by matrix multiplying it with the original known y values
        #NEW: in the window of interest
        c = np.matmul(coeffs,y[i-half_window:i+window-1-half_window+1])
        
        #NEW: c[0] = is the smoothed value for i
        #NEW: we can calculate the interpolated values using y = A2*c
        y_interp = np.matmul(A2,c)
        
        #NEW: rearrange to return - select the values needed for the smoothing
        #NEW: based on the frames that we have sampled
        if N == 1:
            new_data = pd.DataFrame(t2,columns = ["tstep"])
            new_data["frameno"] = new_data["tstep"] + x[i]
            new_data["y_smooth"+str(N)] = y_interp
            new_data = new_data.drop("tstep",axis = 1)
        else:
            sm_data = pd.DataFrame(t2,columns = ["tstep"])
            sm_data["frameno"] = sm_data["tstep"] + x[i]
            sm_data["y_smooth"+str(N)] = y_interp
            sm_data = sm_data.drop("tstep", axis = 1)
            new_data = new_data.merge(sm_data,how='outer',on='frameno')
                
        # Calculate c0 which is also the y value for y[i] based on available only
        y_smoothed[i] = 0
        for j in range(0, window, 1):
            y_smoothed[i] += coeffs[0, j] * y[i + j - half_window]
            
        # If at the end or beginning, store all coefficients for the polynom
        if i == half_window:
            first_coeffs = np.zeros(polynom)
            for j in range(0, window, 1):
                for k in range(polynom):
                    first_coeffs[k] += coeffs[k, j] * y[j]
        elif i == len(x) - half_window - 1:
            last_coeffs = np.zeros(polynom)
            for j in range(0, window, 1):
                for k in range(polynom):
                    last_coeffs[k] += coeffs[k, j] * y[len(y) - window + j]

    # Interpolate the result at the left border
    for i in range(0, half_window, 1):
        y_smoothed[i] = 0
        x_i = 1
        for j in range(0, polynom, 1):
            y_smoothed[i] += first_coeffs[j] * x_i
            x_i *= x[i] - x[half_window]

    # Interpolate the result at the right border
    for i in range(len(x) - half_window, len(x), 1):
        y_smoothed[i] = 0
        x_i = 1
        for j in range(0, polynom, 1):
            y_smoothed[i] += last_coeffs[j] * x_i
            x_i *= x[i] - x[i-half_window - 1]  #♠wrong term subtracted
            
    #NEW: compute interpolated mean to sample when not available
    new_data["mean"] = new_data.iloc[:,1:].mean(axis=1)
    y_all = new_data[["frameno","mean"]]

    return y_smoothed, y_all


def head_withers_diff(nose_y, forehead_y, withers_y):
    
    # calculate head midpoint
    headmid_y = (nose_y + forehead_y)/2
    
    # height of withers = withers_y
    # difference between withers and mid of the head
    headdif = withers_y - headmid_y
    
    return headdif


def angle(p0, p1=np.array([0,0]), p2=None):
    """
    Compute angle (in degrees) for p0p1p2 corner
    
    Parameters
    ----------
    Points in the form of [x,y]
        p0: first point (edge1)
        p1: second point (middle) -> this angle is calculated
        p2: third point (edge2)
    Returns
    -------
    angle of the corner between p0-[^p1]-p2 (in degrees)
    """
    
    if p2 is None:
        p2 = p1 + np.array([1, 0])
    v0 = np.array(p0) - np.array(p1)
    v1 = np.array(p2) - np.array(p1)

    ang = np.math.atan2(np.linalg.det([v0,v1]),np.dot(v0,v1))
    
    return np.degrees(ang)

def norm_3p(p1,p2,p3):
    """
    Compute norm distance for p0p1p2 corner = perpendicular distance from 
    p1 to the line connecting p0 and p2

    Parameters:
    -----------
        p0,p1,p2 - points in the form of [x,y]
    Returns:
    ---------
        d = norm distance
    """
    
    d = norm(np.cross(p3-p1, p1-p2))/norm(p3-p1)
    
    return d


def back_arch(x,y):
    """
    

    Parameters
    ----------
    x : TYPE
        DESCRIPTION.
    y : TYPE
        DESCRIPTION.

    Returns
    -------
    d : TYPE
        DESCRIPTION.
    ang : TYPE
        DESCRIPTION.

    """
    
    p1 = np.array([x[0],y[0]]) # sacrum
    p2 = np.array([x[1],y[1]]) # caudal
    p3 = np.array([x[2],y[2]]) # withers
        
    # first vector = sacrum,caudal -- second vector = caudal, withers
    ang = angle(p1,p2,p3)
    d = norm(np.cross(p3-p1, p1-p2))/norm(p3-p1)
    
    return d, ang
    
    
def stat_hoof(x,y,t, plotbool):
    """
    Returns t, x and y of static hoof positions, based on difference in keypoint 
    as related to t (i.e. frame difference)
        
    Parameters
    ----------
    x : TYPE
        DESCRIPTION.
    y : TYPE
        DESCRIPTION.
    t : TYPE
        DESCRIPTION.

    Returns
    -------
    tstat : TYPE
        DESCRIPTION.
    xstat : TYPE
        DESCRIPTION.
    ystat : TYPE
    """
    
    # # interpolate (x,y) and t
    # xnew,xall = non_uniform_savgol(t,x,9,2)
    # xnew = xall["mean"].to_numpy()
    # ynew,yall = non_uniform_savgol(t,y,9,3)
    # ynew = yall["mean"].to_numpy()
    # tnew = xall["frameno"]
    
    # # interpolation = unreliable at edges (4 first and last samples)    
    # ynew[np.where(ynew < min(y))] = min(y)
    # ynew[np.where(ynew > max(y))] = max(y)
    
    
    # del xall, yall
    xnew = x
    ynew = y
    tnew = t
    
    # calculate position
    d1 = np.diff(xnew)
    d2 = np.diff(ynew)
    
    # calculate distance between two successive points
    dist = np.sqrt(d1**2 + d2**2)
    
    #☻new; add synthetic data to edges to ensure no effect of edges for findpeaks
    dist = np.append([25,24,23,22,21],dist, axis =0 )
    dist = np.append(dist,[20,21,22,23,24], axis =0 )
    
    # determine peaks; take into account 10 steps forwards
    fp = findpeaks(method = 'peakdetect', lookahead = 5, 
                   interpolate = 5,
                   whitelist = 'valley')
    results = fp.fit(dist) 
    dist = dist[5:-5]
    # fp.plot()
    results = results["df"]
    results = results.iloc[5:-5,:].reset_index(drop=1)
    results = results.rename(columns = {'x' : 'tstep', 'y' : 'dist'})
    results.loc[len(results)] = np.nan
    results["frame"] = tnew
    results["xnew"] = xnew
    results["ynew"] = ynew
    
    # filter non-plausible valleys (not interested in these)
    results.loc[(results["valley"] == 1) & \
                (results["dist"] >= 10),"valley"] = 0
    
    # filter non-plausible peaks (not interested in these)
    results.loc[(results["peak"] == 1) & \
                (results["dist"] <= 20),"peak"] = 0
    
    # prepare selection
    results["valno"] = np.nan
    valleys = results.loc[results["valley"] == 1,:].reset_index(drop=1)
    peaks = results.loc[results["peak"] == 1,:].reset_index(drop=1)
    
    # filter / combine valleys in between which no peak is present
    valleys["delcrit"] = 0
    for i in range(len(valleys)-1,0,-1):
        inbetweenpeaks = peaks.loc[(peaks["tstep"] < valleys["tstep"][i]) & \
              (peaks["tstep"] > valleys["tstep"][i-1]),:]
        if len(inbetweenpeaks) == 0:
            valleys["delcrit"][i-1] = 1
    valleys = valleys.loc[valleys["delcrit"]==0,:].reset_index(drop=1)
   
    """
    # # calculate difference between two valleys
    # valleys["framediff"] = valleys["tstep"].diff()
    
    # # shitty piece of zero elegant code to combine valleys
    # for i in range(len(valleys)-1,0,-1):
    #     print(i)
    #     if valleys["framediff"][i] <= 8:  #12
    #         # print(i)
    #         valleys["tstep"][i-1] = (valleys["tstep"][i-1]+ valleys["tstep"][i])/2
    #         # print(valleys)
    """
    
    # delete valleys that are with the new conditions less than 15 difference
    valleys["framediff"] = valleys["tstep"].diff()
    
    
    valleys["delcrit"] = np.nan
    valleys["delcrit"][0] = 0 
    for i in range(1,len(valleys)):
        inbetweenpeaks = peaks.loc[(peaks["tstep"] < valleys["tstep"][i]) & \
              (peaks["tstep"] > valleys["tstep"][i-1]),:]
        if (len(inbetweenpeaks) > 0) & (inbetweenpeaks["dist"].max() > 30) & (valleys["framediff"][i] >= 7) :  #15
            valleys["delcrit"][i] = 0
        else:
            valleys["delcrit"][i] = 1

    # select                   
    valleys = valleys.loc[(valleys["delcrit"] == 0),:]       
    valleys = valleys.reset_index(drop=1)
    valleys["valno"].fillna(value = pd.Series(np.array(range(1,len(valleys)+1))), inplace = True)
    
    # add valley number
    results["distvalley1"] = 100 
    for i in range(0,len(valleys)):
        # add col with distance to closest valley
        results["distvalley2"] = (results["tstep"] - valleys["tstep"][i]).abs()
        refdist = valleys.loc[valleys["valno"] == valleys["valno"][i],"dist"].values
        results.loc[((results["dist"] < 10) | (results["dist"] < 1.5*refdist.item()))  & \
                    (results["distvalley2"] < results["distvalley1"]), "valno"] = valleys["valno"][i]
        results["distvalley1"] = results["distvalley2"]
    
    # bridge gaps if gap < 3
    # delete values for which gap > 3 -- 
    for i in range(0,len(valleys)):
        allvalleys = results.loc[results["valno"]== valleys["valno"][i]]
        allvalleys["gap"] = allvalleys["tstep"].diff()
        idx = allvalleys.loc[allvalleys["gap"]>3,"valno"]
        if len(idx) > 0:    
            idx = idx.index.values[0]
            results.loc[(results.index.values >= idx) & (results["valno"] == valleys["valno"][i]),"valno"]=np.nan
    
    #assign valno
    results["valno"].interpolate(method = "linear",
                            inplace = True,
                            limit = 3,
                            limit_area = "inside")
    # avoid extrapolation behind valley
    results.loc[(round(results["valno"]) != results["valno"]),"valno"] = np.nan
    results.loc[results["dist"]>40,"valno"] = np.nan
    
    # delete labx, distvalley1, distvalley 2
    results.drop(["labx","distvalley1","distvalley2"], axis = 1, inplace = True )
    
    if plotbool == True:
        # fp.plot()
        plt.rc('font', size=15) #controls default text size
        fig, ax = plt.subplots(figsize = (28,20), nrows = 2, ncols = 2)
        ax[0,0].plot(t,x,'o:',
                   markersize = 8, 
                   linewidth = 2,
                   color = 'darkmagenta'
                   )
        ax[0,0].plot(tnew,xnew, 'o:',
                   markersize = 5, 
                   linewidth = 2,
                   color = 'mediumblue'
                   )
        ax[1,0].plot(t,y, 'o:',
                    markersize = 8, 
                    linewidth = 2,
                    color = 'darkmagenta'
                    )
        ax[1,0].plot(tnew,ynew, 'o:',
                   markersize = 5, 
                   linewidth = 2,
                   color = 'mediumblue'
                   )
        ax[0,0].set_title("(x) position of the keypoints across frames")
        ax[0,0].set_ylabel("x")
        ax[0,0].set_xlabel("frame")
        ax[1,0].set_title("(y) position of the keypoints across frames")
        ax[1,0].set_ylabel("y")
        ax[1,0].set_xlabel("frame")
        ax[0,0].legend(["interpolated","smoothed"])
        ax[1,0].legend(["interpolated","smoothed"])
        
        ax[0,1].plot(xnew,ynew,'o:',
                   markersize = 8,
                   lw = 2,
                   color = "orangered")
        ax[0,1].set_title("(x,y) position of the keypoints across frames")
        ax[0,1].set_ylabel("y")
        ax[0,1].set_xlabel("x")
        ax[1,1].plot(tnew[:-1],dist,'o:',
                   markersize = 8,
                   lw = 2,
                   color = "darkcyan")
        
        #  cmap = plt.cm.get_cmap("Dark2") 
        #normalize item number values to colormap
        norm = pltcol.Normalize(vmin=0, vmax =results["valno"].max())

        for valno in range(1,int(results["valno"].max())+1):
            rgba_color = cm.Dark2(norm(valno-1),bytes=True) 
            rgba_color = tuple(ti/255 for ti in rgba_color)
            ax[1,1].plot(results.loc[(results["valno"] == valno),"frame"],
                         results.loc[(results["valno"] == valno),"dist"],
                         'X',
                         markersize = 15,
                         color = rgba_color
                         )
        ax[1,1].set_title("Difference in distance (x,y) of keypoints  between frames")
        ax[1,1].set_ylabel("pixels")
        ax[1,1].set_xlabel("frame")
        
    return results


def combine_hoof_stats(cow,results_LF, results_RF, results_LH, results_RH):
    # convert cow to dataframe length results_x
    cowcol = pd.DataFrame(np.repeat(cow,len(results_LH)).T, columns = ['cow'])
    # drop columns not needed + combine results in single dataframe
    results_LH.drop(["tstep","dist"], inplace = True, axis = 1)
    results_LH = results_LH.rename(columns = {'valley' : 'val_LH', 'peak' : 'peak_LH',
                                           'xnew' : 'x_LH', 'ynew' : 'y_LH', 
                                           'valno' : 'valno_LH'})
    results_RH.drop(["tstep","dist"], inplace = True, axis = 1)
    results_RH = results_RH.rename(columns = {'valley' : 'val_RH', 'peak' : 'peak_RH',
                                           'xnew' : 'x_RH', 'ynew' : 'y_RH', 
                                           'valno' : 'valno_RH'})
    results_LF.drop(["tstep","dist"], inplace = True, axis = 1)
    results_LF = results_LF.rename(columns = {'valley' : 'val_LF', 'peak' : 'peak_LF',
                                           'xnew' : 'x_LF', 'ynew' : 'y_LF', 
                                           'valno' : 'valno_LF'})
    results_RF.drop(["tstep","dist"], inplace = True, axis = 1)
    results_RF = results_RF.rename(columns = {'valley' : 'val_RF', 'peak' : 'peak_RF',
                                           'xnew' : 'x_RF', 'ynew' : 'y_RF', 
                                           'valno' : 'valno_RF'})
    statgait = pd.concat([cowcol,results_LH,results_RH,results_LF,results_RF],axis = 1)
    statgait.drop("frame", inplace = True, axis = 1)
    statgait["frame"] = results_LH["frame"]
    cols = list(statgait.columns.values) # make a list of all of the columns in the df
    cols.pop(cols.index('cow')) # remove cow from list
    cols.pop(cols.index('frame')) #Remove frame from list
    statgait = statgait[['cow', 'frame'] + cols]
    
    return statgait



# gait features from stationarity of hoofs
def strides(df):
    """
    Stride length = distance and time taken for 1 stride with one leg

    Parameters
    ----------
    df = DataFrame
        contains the statgait features of all 4 hoofs (or less) for one cow

    Returns
    -------
    strides : DatFrame
        Contains 4 rows, one for each hoof with following traits:
            step duration
            step length
            ground duration
            moving duration
            moving distance
            number of steps with hind legs
            3 leg ground time
            tracking

    """
    df = df.reset_index(drop=1)
    
    
    cow = df["cow"][0]
    strides = pd.DataFrame([],columns = ["cow", "hoof", "strideduration", 
                                         "stridelength", "groundduration",
                                         "movingduration","movingdistance",
                                         "no_strides", "leg3time","tracking"])
    
    for hoof in ["LH","RH","LF","RF"]:
        # try:
        # print(hoof)
        var1 = "valno_"+hoof
        var2 = "x_"+hoof
        ds = df[["frame",var1,var2]]
        test = ds[[var1,var2]].groupby(by = var1).count()
        test = test.loc[test[var2]==1].reset_index()
        if len(test) > 0:
            for i in range(0,len(test)):
                idx = ds.loc[ds[var1] == test[var1][i]].index.values
                ds[var1][idx+1] = test[var1][i]
        
        #start of a new step
        starts = ds[var1].dropna().drop_duplicates()
        ends = ds.sort_values(by = "frame", axis = 0, na_position = 'first', ascending = False)
        ends = ends[var1].dropna().drop_duplicates().sort_values()
                    
        # combine and calculate start and end for the duration
        steps = ds.iloc[starts.index,:]
        steps = steps.append(ds.iloc[ends.index,:]).sort_values(by='frame')
    
        # DURATION = calculate frame difference  end of each step      
        strideduration = round(steps["frame"][1::2].diff().mean(),3)  # mean duration of full step
        
        # LENGTH of full step  - with goniometry, this can be transferred to distances
        stridelength = round(steps[var2][1::2].diff().mean(),3)  # mean duration of full step
        
        # GROUND TIME = duration in frames that leg is on the ground
        #TODO: what if incomplete steps or crappy data?
        steps["groundtime"] = steps[[var1,"frame"]].groupby(var1).diff()
        
        # delete steps that might originate from crappy detection in corners -- if less long than 15% of max
        m = steps["groundtime"].max()*0.15
        groundduration = round(steps.loc[steps["groundtime"] > m, "groundtime"].mean(),3)  # time hoof is on the ground in frames
        
        # MOVING DURATION = duration in frames that leg is moving = ends - starts
        steps["movingindex"] = steps[var1].diff()
        steps.loc[(steps["movingindex"] == 0),"movingindex"] = np.nan
        steps.loc[(~steps["movingindex"].isna()),"movingindex"] = steps.loc[(~steps["movingindex"].isna()),var1]
        steps["movingindex"].fillna(method = 'bfill', limit = 1, inplace = True)
        steps["movingdur"] = steps[["movingindex","frame"]].groupby("movingindex").diff()
        movingduration = round(steps["movingdur"].mean(),3)
        
        # MOVING DISTANCE = distance over which that leg is moving approx. the same as total distance
        steps["movingdist"] = steps[["movingindex",var2]].groupby("movingindex").diff()
        movingdist = round(steps["movingdist"].mean(),3)        
    # except:
        #     strideduration = np.nan
        #     stridelength = np.nan
        #     groundduration = np.nan
        #     movingduration = np.nan
        #     movingdist = np.nan
        
        #----------------------------------------------------------------------
        # also calculate 3 hoof ground time when LH and RH
        if hoof in ["LH","RH"]:
            # select all data except for the data from that hoof
            cols = list(df.columns.values) # make a list of all of the columns in the df
            cols = [i for i in cols if ((hoof not in i) and ("valno" in i)) or ("frame" in i)] # remove all data from hoof from list
            ds = df[cols]
    
            # find periods that all three hoofs are stationary = on the ground
            ds["summed"] = ds.iloc[:,[1, 2, 3]].sum(axis = 1, skipna = False)
            counts = ds[["summed","frame"]].groupby(by = "summed").count()
            
            # add features
            no_strides = len(counts)
            leg3time = round(counts["frame"].mean(),3)
            if no_strides == 0:
                leg3time = 0
        else:
            no_strides = np.nan
            leg3time = np.nan
        
        #----------------------------------------------------------------------
        # tracking up
        #    difference in x-position LEFT legs touching ground
        #    difference in x-position RIGHT legs touching ground 
        if hoof in ["LH","RH"]:
            # select all data from left or from right
            cols = list(df.columns.values) # make a list of all of the columns in the df
            cols = [i for i in cols if ((hoof[0] in i) and (("x" in i) or ("valno") in i)) or ("frame" in i)] # select data from side from list
            ds = df[cols]
            var1a = "x_" + hoof
            var1b = "valno_" + hoof
            var2a = "x_" + hoof[0] + "F"
            var2b = "valno_" + hoof[0] + "F"
            
            # calculate mean x when stable leg position
            legpos_hind = ds[[var1a,var1b]].groupby(by = var1b).mean().reset_index()
            legpos_front =  ds[[var2a,var2b]].groupby(by = var2b).mean().reset_index()
            legpos_front["hind"] = np.nan
            
            # for each stride with the front leg, find corresponding hind position based on distance
            for s in range(0,len(legpos_front)):
                #print(s)
                # legpos_hind["diff"] = (legpos_front[var2a][s] - legpos_hind[var1a])
                # legpos_hind.loc[(legpos_hind["diff"] < -300) |  \
                #                 (legpos_hind["diff"] > 150), "diff"] = np.nan
                # legpos_front["hind"][s] = \
                #        legpos_hind.loc[~legpos_hind["diff"].isna(),var1a].sum(skipna = True)
                legpos_hind["diff"] = (legpos_front[var2a][s] - legpos_hind[var1a]).abs()
                legpos_front["hind"][s] = legpos_hind.loc[legpos_hind["diff"] == legpos_hind["diff"].min(),var1a] 
            legpos_front.loc[legpos_front["hind"] == 0, "hind"] = np.nan
            
            # average difference
            tracking = round((legpos_front["hind"] - legpos_front[var2a]).mean(),3)
        else:
            tracking = np.nan
            
        
        # add to strides 
        newline = {"cow" : cow,
                   "hoof" : hoof,
                   "strideduration": strideduration,
                   "stridelength": stridelength,
                   "groundduration": groundduration,
                   "movingduration": movingduration,
                   "movingdistance": movingdist,
                   "no_strides" : no_strides,
                   "leg3time" : leg3time,
                   "tracking" : tracking}
        strides = strides.append(newline, ignore_index = True)
    
        #TODO: calculate x values difference (correct distance diversification camera)
    
    return strides

