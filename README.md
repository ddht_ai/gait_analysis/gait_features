# KB DDHT AI gait features
> _Created_: January 11, 2022  
> _Created by_: Ines Adriaens (adria036)  
> _Lead_: Ines Adriaens and Marjaneh Taghavirazavizadeh

## Project description and aim

This project aims at developing machine vision solutions for automated gait analysis of dairy cows, using video data recorded at the Dairy Campus in Leeuwarden, NL. The model will be based on key point detection of cows coming from the milking parlour. 

Moved from NLAS. Resources: T-LEAP model developed by Farm Technology (Helena). 


## Workflow
All scripts will be developed in Python-Spyder / anaconda environment. Initially, annotated data will be used awaiting the automatically detected change points as developed by Marjaneh (see repository "keypoint_detection" in this subgroup).
  
1. Data preparation (several rounds necessary)
2. Data exploration: variability, frame rate selection, missing data etc.
3. Proposing gait features + calculate from annotated data
4. Repeatability

## Scripts

1. gait_features2.py: this returns the gait features based on the output of T-LEAP, with different post-processing steps.
2. gait_feat_funct.py: this contains all the necessary functions for the calculations

## workflow
Before you begin, first change the input (data) and output paths (where you want to store the figures and output). If you want to plot, set "plotbool" to True.